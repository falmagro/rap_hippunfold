#!/bin/bash

set -x

for elem in `cat $1` ; do
   cad="";
   for fil in `cat ~/rap_hippunfold/files.txt` ; do
      t=`echo $fil |sed "s|PLACEHOLDER|$elem|g"`;
      if [ -e $HOME/output/$t ] ; then
         cad="$cad $HOME/output/$t" ;
      fi  ;
   done
   echo $cad
   zip -r "$elem.zip" $cad
   dx upload $elem.zip --path FBP_TESTS_2:hippunfold_outputs_2/
done