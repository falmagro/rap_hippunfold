#!/bin/bash

set -x

cad=`awk 'BEGIN { ORS = " " } { print }' subj.txt`

date
docker run --rm -e TAR_OPTIONS='--no-same-owner' \
  -v /home:/home \
  khanlab/hippunfold:latest \
  /home/dnanexus/data /home/dnanexus/output \
  participant --modality T1w --participant_label $cad \
  --cores all --output-density 0p5mm 2mm unfoldiso
date
