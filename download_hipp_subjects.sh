#!/bin/bash

set -x

cad=""
new_nam=`echo $1 | sed 's|.txt||g'`

for SUBJECT in `cat $1` ; do

   export ID=${SUBJECT:1:10}
   export VISIT=${SUBJECT:0:1}
   export START=${SUBJECT:1:2}
   export DNADirectory=FBP_TESTS_2

   date;
   echo $SUBJECT

   d1="${DNADirectory}:Bulk/Brain\ MRI/"
   d2="/$START/${ID}_202"
   d3="_${VISIT}_0.zip"
   d4="/$START/${ID}_2630"
   mkdir $SUBJECT;
   cd $SUBJECT;
   d5="T1/${d2}52$d3"

   if [ ! -d sub-$SUBJECT ] ; then
     dx download "${d1}${d5}";
     for elem in *.zip ; do
        unzip $elem;
        rm $elem ;
     done
     cd ..

     mv $SUBJECT sub-$SUBJECT;

     cd sub-$SUBJECT

     mkdir anat/

     cp T1/T1.nii.gz anat/sub-${SUBJECT}_T1w.nii.gz
     cp T1/T1.json   anat/sub-${SUBJECT}_T1w.json

     cd ../
   fi
done

